package com.example.students;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class ViewStudent extends AppCompatActivity {
    private String name;
    private String id;
    private String phone;
    private String address;
    private Boolean checked;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student);
        setUpListeners();
        Intent intent = getIntent();
        this.name = intent.getStringExtra("name");
        this.id = intent.getStringExtra("id");
        this.phone = intent.getStringExtra("phone");
        this.address = intent.getStringExtra("address");
        this.checked = intent.getBooleanExtra("checked", false);
        this.position = intent.getIntExtra("position", 0);

        Log.d("TAG", "view: " + position);


        TextView studentName = findViewById(R.id.currentStudentname);
        TextView studentPhone = findViewById(R.id.currentStudentPhone);
        TextView studentAddress = findViewById(R.id.currentStudentAddress);
        TextView studentId = findViewById(R.id.currentStudentid);
        CheckBox studentChecked = findViewById(R.id.viewChecked);

        studentName.setText(this.name);
        studentId.setText(this.id);
        studentPhone.setText(this.phone);
        studentAddress.setText(this.address);
        studentChecked.setChecked(this.checked);
    }

    private void setUpListeners(){
        Button editButton = findViewById(R.id.viewEdit);
        editButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i = new Intent( getApplicationContext(), EditStudent.class);
                i.putExtra("name", name);
                i.putExtra("id", id);
                i.putExtra("phone", phone);
                i.putExtra("address", address);
                i.putExtra("checked", checked);
                i.putExtra("position", position);
                startActivity(i);
            }
        });
    }
}