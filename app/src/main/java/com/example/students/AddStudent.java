package com.example.students;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.students.model.Student;

public class AddStudent extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        setUpListeners();
    }

    private void setUpListeners(){
        Button saveButton = findViewById(R.id.addSave);
        Button cancelButton = findViewById(R.id.addCancel);
        saveButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                addStudent();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                finish();
            }
        });
    }

    private void addStudent(){
        TextView name = findViewById(R.id.addName);
        TextView id = findViewById(R.id.addId);
        TextView phone = findViewById(R.id.addPhone);
        TextView address = findViewById(R.id.addAddress);
        CheckBox checked = findViewById(R.id.addChecked);
        int position = MainActivity.studentsList.size();
        MainActivity.studentsList.add(position, new Student(
                name.getText().toString(),
                id.getText().toString(),
                phone.getText().toString(),
                address.getText().toString(),
                checked.isChecked()
        ));
        MainActivity.studentsAdapter.notifyItemInserted(position);
        finish();
    }
}