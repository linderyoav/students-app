package com.example.students;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.example.students.adapter.StudentsAdapter;
import com.example.students.model.Student;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView studentsListRecycleView;
    public static StudentsAdapter studentsAdapter;
    public static ArrayList<Student> studentsList = new ArrayList<Student>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpListeners();

        this.studentsListRecycleView = findViewById(R.id.studentList);
        this. studentsAdapter = new StudentsAdapter(studentsList, getApplicationContext());
        studentsListRecycleView.setAdapter(studentsAdapter);
        studentsListRecycleView.setLayoutManager(new LinearLayoutManager(this));
    }

    private void setUpListeners(){
        ImageView addButton = findViewById(R.id.add);
        addButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i = new Intent(getApplicationContext(), AddStudent.class);
                startActivity(i);
            }
        });
    }
}