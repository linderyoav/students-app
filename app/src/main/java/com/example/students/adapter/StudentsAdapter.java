package com.example.students.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.students.AddStudent;
import com.example.students.MainActivity;
import com.example.students.R;
import com.example.students.ViewStudent;
import com.example.students.model.Student;

import java.util.ArrayList;

public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.ViewHolder> {
    private ArrayList<Student> studentsList;
    private Context context;

    public StudentsAdapter(ArrayList<Student> studentsList, Context context){
        this.studentsList = studentsList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View studentsView = inflater.inflate(R.layout.student_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(studentsView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d("TAG", "shamla: " + position);
        Student student = studentsList.get(position);

        ImageView studentPic = holder.studentPic;
        TextView studentName = holder.studentName;
        TextView studentId = holder.studentId;
        CheckBox studentChecked = holder.studentChecked;
        studentName.setText(student.getName());
        studentId.setText(student.getId());
        studentChecked.setChecked(student.getChecked());
        studentPic.setImageResource(R.drawable.student);

        studentChecked.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
               @Override
               public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                   MainActivity.studentsList.get(position).setChecked(isChecked);
                   studentsList.get(position).setChecked(isChecked);
               }
           }
        );

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent i = new Intent( context, ViewStudent.class);
                i.putExtra("name", studentName.getText().toString());
                i.putExtra("id", studentId.getText().toString());
                i.putExtra("phone", student.getPhone());
                i.putExtra("address", student.getAddress());
                i.putExtra("checked", studentChecked.isChecked());
                i.putExtra("position", position);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return studentsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView studentPic;
        public TextView studentName;
        public TextView studentId;
        public CheckBox studentChecked;

        public ViewHolder(View itemView){
            super(itemView);
            studentPic = (ImageView) itemView.findViewById(R.id.studentPic);
            studentName = (TextView) itemView.findViewById(R.id.studentName);
            studentId = (TextView) itemView.findViewById(R.id.studentId);
            studentChecked = (CheckBox) itemView.findViewById(R.id.studentChecked);
        }

    }
}
