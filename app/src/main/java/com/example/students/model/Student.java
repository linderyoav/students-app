package com.example.students.model;

import android.graphics.Bitmap;

public class Student {

    private Bitmap pic;
    private String name;
    private String id;
    private String phone;
    private String address;
    private boolean checked;


    public Student(String name, String id, String phone, String address, boolean checked) {
//        this.pic = pic;
        this.name = name;
        this.id = id;
        this.phone = phone;
        this.address = address;
        this.checked = checked;
    }

    public void setStudent(String name, String id, String phone, String address, boolean checked){
        this.name = name;
        this.id = id;
        this.phone = phone;
        this.address = address;
        this.checked = checked;
    }

    public void setPic(Bitmap pic){
        this.pic = pic;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setPhone(String phone){
        this.phone = phone;
    }

    public void setAddress(String address){
        this.address = address;
    }
    public void setChecked(boolean checked){
        this.checked = checked;
    }

    public Bitmap getPic(){
        return this.pic;
    }

    public String getName(){
        return this.name;
    }

    public String getId(){
        return this.id;
    }
    public String getPhone(){
        return this.phone;
    }
    public String getAddress(){
        return this.address;
    }

    public boolean getChecked(){
        return this.checked;
    }



}
