package com.example.students;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class EditStudent extends AppCompatActivity {
    private int position;
    private TextView nameTextView;
    private TextView idTextView;
    private TextView phoneTextView;
    private TextView addressTextView;
    private CheckBox checkedCheckBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_student);
        Intent intent = getIntent();
        this.nameTextView = findViewById(R.id.editName);
        this.idTextView  = findViewById(R.id.editId);
        this.phoneTextView = findViewById(R.id.editPhone);
        this.addressTextView = findViewById(R.id.editAddress);
        this.checkedCheckBox = findViewById(R.id.editChecked);

        this.nameTextView.setText(intent.getStringExtra("name"));
        this.idTextView.setText(intent.getStringExtra("id"));
        this.phoneTextView.setText(intent.getStringExtra("phone"));
        this.addressTextView.setText(intent.getStringExtra("address"));
        this.checkedCheckBox.setChecked(intent.getBooleanExtra("checked", false));
        this.position = intent.getIntExtra("position", 0);
        setUpListeners();

    }

    private void setUpListeners() {
        Button saveButton = findViewById(R.id.editSave);
        Button cancelButton = findViewById(R.id.editCancel);
        Button deleteButton = findViewById(R.id.editDelete);
        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editStudent();
                Intent i = new Intent( getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MainActivity.studentsList.remove(position);
                MainActivity.studentsAdapter.notifyItemRemoved(position);
                Intent i = new Intent( getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });
    }

    private void editStudent(){

        MainActivity.studentsList.get(position).setStudent(
                this.nameTextView.getText().toString(),
                this.idTextView.getText().toString(),
                this.phoneTextView.getText().toString(),
                this.addressTextView.getText().toString(),
                this.checkedCheckBox.isChecked()
        );
        MainActivity.studentsAdapter.notifyItemChanged(position);
    }
}